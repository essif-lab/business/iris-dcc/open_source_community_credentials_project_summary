An open-source Discourse plugin that allows community-friendly transparent recognition, award and governance of verifiable credentials as represented by user-friendly 'badges'.  Simple agreements may be formed in a forum discussion and sealed in a ‘digital handshake’ between verified human vc/badge holders.

1. Verifiable credentials mapped as ‘proven capability’ badges
2. Portable ‘badge’ credentials, exported and verifiable across communities
3. Simple ‘digital handshake’ VC- backed agreements between parties, verified access to protected services, ticketing, gig entry and third-party fulfilment.



